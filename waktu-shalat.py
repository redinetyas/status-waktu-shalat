#! /usr/bin/python

from jadwal_shalat import shalat
from datetime import datetime as date
from hijri_converter import Hijri as H
from os import system
from threading import Thread as thred

class waktuShalat:
    def __init__(self):
        jadwal = shalat()
        self.adzan = "/home/nestero/.config/dwm/status\ bar/dwm-status/adzan.mp3"
        self.adzan_Subuh = "/home/nestero/.config/dwm/status\ bar/dwm-status/adzan_subuh.mp3"
        self.shalat = jadwal.jadwal()
        self.hari_Ini = H.today()

    def hijri(self,):
        bulan = self.hari_Ini.month_name()
        tahun = self.hari_Ini.datetuple()
        today = tahun[2]
        tahun = tahun[0]
        tb = f"{today} {bulan} {tahun} H"
        return tb
    
    def adhan(self, x):
        if x == "adzan":
            system(f"mpv {self.adzan} > /dev/null 2>&1")
        elif x == "adzan subuh":
            system(f"mpv {self.adzan_Subuh} > /dev/null 2>&1")

    def waktu(self, waktu):
        shalat = {
                "Imsak" : self.shalat[2],
                "Subuh" : self.shalat[3],
                "Terbit" : self.shalat[4],
                "Dhuha" : self.shalat[5],
                "Dzuhur" : self.shalat[6],
                "Ashar" : self.shalat[7],
                "Maghrib" : self.shalat[8],
                "Isya" : self.shalat[9],
                }
        return shalat[waktu]

    def timer(self, sekarang, waktu):
        x = sekarang
        y = waktu
        x = x.split(":")
        x = int(x[0])*60 + int(x[1])
        y = y.split(":")
        y = int(y[0])*60 + int(y[1])
        z = y - x
        jam = z / 60
        menit = z % 60
        if int(jam) < 10:
            jam = f"0{int(jam)}"
        else:
            jam = int(jam)
        if menit < 10:
            menit = f"0{menit}"
        return f"{jam}:{menit}"

    def getWaktu(self, sekarang):
        shalat = ["Subuh", "Dhuha", "Dzuhur", "Ashar", "Maghrib", "Isya"]
        waktu = f"{self.hijri()}"
        for s in shalat:
            if sekarang == self.waktu(s):
                waktu = f"Waktu Shalat {s}"
            elif sekarang >= "03:00" and sekarang < self.waktu("Imsak"):
                timer = self.timer(sekarang, self.waktu("Imsak"))
                waktu = f"Imsak {timer} | {self.hijri()}"
            elif sekarang == self.waktu("Imsak"):
                waktu = "Imsak, Lekas Sahur"
            elif sekarang > self.waktu("Subuh") and sekarang < self.waktu("Dhuha"):
                timer = self.timer(sekarang, self.waktu("Terbit"))
                waktu = f"Terbit {timer} | {self.hijri()}"
            else:
                if sekarang > self.waktu("Imsak") and sekarang < self.waktu("Subuh"):
                    timer = self.timer(sekarang, self.waktu("Subuh"))
                    waktu = f"Subuh {timer} | {self.hijri()}"
                else:
                    for w in range(0, 5):
                        if sekarang > self.waktu(shalat[w]) and sekarang < self.waktu(shalat[w+1]):
                            timer = self.timer(sekarang, self.waktu(shalat[w+1]))
                            waktu = f"{shalat[w+1]} {timer} | {self.hijri()}"
        return waktu
    
    def alaram(self):
        jam = date.now()
        pukul = jam.strftime("%R")
        shalat = ["Dzuhur", "Ashar", "Maghrib", "Isya"]
        if pukul == self.waktu(shalat[0]) or pukul == self.waktu(shalat[1]) or pukul == self.waktu(shalat[2]) or pukul == self.waktu(shalat[3]):
            thred(target = print(f"  {self.getWaktu(pukul)} ")).start()
            thred(target = self.adhan("adzan")).start()
        elif pukul == self.waktu("Subuh"):
            thred(target = print(f"  {self.getWaktu(pukul)} ")).start()
            thred(target = self.adhan("adzan subuh")).start()
        else:
            print(f"  {self.getWaktu(pukul)} ")

Alaram = waktuShalat()
Alaram = Alaram.alaram()
